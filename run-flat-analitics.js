var axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-analytics', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// function to download content
let download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`./images/${filename}`)).on('close', callback);
  });
};

computeImagePartUrl = function(url) {
  let start = url.lastIndexOf("-");
  return url.substring(start);
};

const Flat = sequelize.define('flat-analytics-new-12-05', {
  uid: Sequelize.STRING,
  type: Sequelize.STRING,
  rooms: Sequelize.FLOAT,
  lSM: Sequelize.FLOAT,
  kSM: Sequelize.FLOAT,
  sM: Sequelize.FLOAT,
  price: Sequelize.INTEGER,
  priceItem: Sequelize.FLOAT,
  districtName: Sequelize.STRING,
  streetName: Sequelize.STRING,
  streetId: Sequelize.STRING,
  lat: Sequelize.STRING,
  lng: Sequelize.STRING,
  wallType: Sequelize.STRING,
  floor: Sequelize.FLOAT,
  beautifulUrl: Sequelize.STRING,
});

var Locations = [];

for (let i = 0; i < 10; i++) {
  setTimeout(() => {
    let searchUrl = `https://dom.ria.com/node/searchEngine/v2/?page=${i}&limit=20&from_realty_id=&to_realty_id=&sort=inspected_sort&user_id=&category=1&realty_type=2&operation_type=1&state_id=12&characteristic%5B209%5D%5Bfrom%5D=&characteristic%5B209%5D%5Bto%5D=&characteristic%5B214%5D%5Bfrom%5D=&characteristic%5B214%5D%5Bto%5D=&characteristic%5B216%5D%5Bfrom%5D=&characteristic%5B216%5D%5Bto%5D=&characteristic%5B218%5D%5Bfrom%5D=&characteristic%5B218%5D%5Bto%5D=&characteristic%5B227%5D%5Bfrom%5D=&characteristic%5B227%5D%5Bto%5D=&characteristic%5B228%5D%5Bfrom%5D=&characteristic%5B228%5D%5Bto%5D=&characteristic%5B1607%5D%5Bfrom%5D=&characteristic%5B1607%5D%5Bto%5D=&characteristic%5B1608%5D%5Bfrom%5D=&characteristic%5B1608%5D%5Bto%5D=&characteristic%5B234%5D%5Bfrom%5D=&characteristic%5B234%5D%5Bto%5D=&characteristic%5B242%5D=239&characteristic%5B265%5D=0&realty_id_only=&date_from=&date_to=&with_phone=&exclude_my=&new_housing_only=&banks_only=&_csrf=xaxsjBpL-r_SgRE2GRxmcgUQPENE37cBwiJo&reviewText=&email=&period=0`;

    let Items = [];
    return axios.get(searchUrl)
      .then(function (response) {
        response.data.items.map(item => Items.push(item));
        console.log("tick");
        console.log(Items);

        for(let t = 0;t < Items.length;t++) {
          setTimeout(() => {
            let flatURL = `https://dom.ria.com/node/searchEngine/v2/view/realty/${Items[t]}?lang_id=2`;
            return axios.get(flatURL)
              .then(function (res) {
                let data = res.data;

                if (data.latitude) {
                  Locations.push({lat: +data.latitude, lng: +data.longitude})
                }

                sequelize.sync()
                  .then(() => Flat.create({
                    uid: data.realty_id,
                    type: data.type,
                    rooms: +data.rooms_count,
                    sM: +data.total_square_meters,
                    price: +data.price,
                    priceItem: +data.price_item,
                    districtName: data.district_name,
                    streetName: data.street_name,
                    streetId: data.street_id,
                    lat: data.latitude,
                    lng: data.longitude,
                    wallType: data.wall_type,
                    floor: data.floor,
                    lSM: data.living_square_meters,
                    kSM: data.kitchen_square_meters,
                    beautifulUrl: data.beautiful_url
                  }));
              })
              .catch(function (error) {
                console.log(error);
              });
          }, 100*(t+1))
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }, 1000*i);
  /*setTimeout(() => {
    fs.writeFile('locations2.js', util.inspect(Locations) , 'utf-8');
    return console.log(Locations);
  }, 10000)*/

}

/*setTimeout(() => {
	fs.writeFileSync('items.js', util.inspect(Items) , 'utf-8');
	return console.log(Items);
}, 10000) */
