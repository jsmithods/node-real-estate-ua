var axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-test', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// function to download content
let download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`./images/${filename}`)).on('close', callback);
  });
};

computeImagePartUrl = function(url) {
  let start = url.lastIndexOf("-");
  return url.substring(start);
};

const FlatImage = sequelize.define('flat-image', {
  uid: Sequelize.STRING,
});

for (let i = 0; i < 1; i++) {
	setTimeout(() => {
		let searchUrl = `https://dom.ria.com/node/searchEngine/v2/?page=${i}&limit=20&from_realty_id=&to_realty_id=&sort=inspected_sort&user_id=&category=1&realty_type=2&operation_type=1&state_id=12&characteristic%5B209%5D%5Bfrom%5D=&characteristic%5B209%5D%5Bto%5D=&characteristic%5B214%5D%5Bfrom%5D=&characteristic%5B214%5D%5Bto%5D=&characteristic%5B216%5D%5Bfrom%5D=&characteristic%5B216%5D%5Bto%5D=&characteristic%5B218%5D%5Bfrom%5D=&characteristic%5B218%5D%5Bto%5D=&characteristic%5B227%5D%5Bfrom%5D=&characteristic%5B227%5D%5Bto%5D=&characteristic%5B228%5D%5Bfrom%5D=&characteristic%5B228%5D%5Bto%5D=&characteristic%5B1607%5D%5Bfrom%5D=&characteristic%5B1607%5D%5Bto%5D=&characteristic%5B1608%5D%5Bfrom%5D=&characteristic%5B1608%5D%5Bto%5D=&characteristic%5B234%5D%5Bfrom%5D=&characteristic%5B234%5D%5Bto%5D=&characteristic%5B242%5D=239&characteristic%5B265%5D=0&realty_id_only=&date_from=&date_to=&with_phone=&exclude_my=&new_housing_only=&banks_only=&_csrf=xaxsjBpL-r_SgRE2GRxmcgUQPENE37cBwiJo&reviewText=&email=&period=0`;

		let Items = [];
		return axios.get(searchUrl)
		  .then(function (response) {
		  	response.data.items.map(item => Items.push(item));
		    console.log("tick");
		    console.log(Items);

		    for(let t = 0;t < Items.length;t++) {
		    	setTimeout(() => {
					let flatURL = `https://dom.ria.com/node/searchEngine/v2/view/realty/${Items[t]}?lang_id=2`;
					return axios.get(flatURL)
						  .then(function (res) {
						    let data = res.data;
						    // let imagesArray = [];
						    let pictures = res.data.photos;
						    let beautifulUrlPhoto = res.data.beautiful_url;

						    let imageCount = 0;

						    for(let image in pictures) {
						    	setTimeout(() => {

							    	return sequelize.sync()
								        .then(() => FlatImage.create({
								          uid: image
								        }));
						    	}, 100*(imageCount+1))
						    	imageCount++;
						    }
						  })
						  .catch(function (error) {
						  	console.log(error);
						  });
		    	}, 1000*(t+1))
		    	
		    	
		    }

		  })
		  .catch(function (error) {
		    console.log(error);
		  });
	}, 3500*(i+1));

	}

/*setTimeout(() => {
	fs.writeFileSync('items.js', util.inspect(Items) , 'utf-8');
	return console.log(Items);
}, 10000) */
