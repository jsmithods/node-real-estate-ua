var axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-analytics', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const Flat = sequelize.define('flat-analytics-new-11-05', {
  uid: Sequelize.STRING,
  type: Sequelize.STRING,
  rooms: Sequelize.STRING,
  lSM: Sequelize.STRING,
  kSM: Sequelize.STRING,
  sM: Sequelize.STRING,
  price: Sequelize.STRING,
  priceItem: Sequelize.STRING,
  districtName: Sequelize.STRING,
  streetName: Sequelize.STRING,
  streetId: Sequelize.STRING,
  lat: Sequelize.STRING,
  lng: Sequelize.STRING,
  wallType: Sequelize.STRING,
  floor: Sequelize.STRING,
  beautifulUrl: Sequelize.STRING,
});

let MapsLatLng = [];

Flat.all({
  attributes: ['lat', 'lng']
}).then(myTableRows => {
  // console.log(myTableRows[0].uid, myTableRows[0].type);
  for (let m = 0; m < myTableRows.length; m++) {
    if (myTableRows[m].lat) {
      MapsLatLng.push({lat: +myTableRows[m].lat,lng: +myTableRows[m].lng})
    }
  }
  console.log(MapsLatLng.length);
  return MapsLatLng;
})
  .then(() => fs.writeFile('locations2.js', util.inspect(MapsLatLng, { maxArrayLength: null }) , 'utf-8')
  );

// Quick example
sequelize.query("SELECT COUNT(`id`) FROM `flat-analytics-new-11-05s` WHERE `districtName`=\"Киевский\"").then(myTableRows => {
  console.log(myTableRows)
});
