var axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-analytics', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const Flat = sequelize.define('flat-analytics-new-11-05', {
  uid: Sequelize.STRING,
  type: Sequelize.STRING,
  rooms: Sequelize.STRING,
  lSM: Sequelize.STRING,
  kSM: Sequelize.STRING,
  sM: Sequelize.STRING,
  price: Sequelize.STRING,
  priceItem: Sequelize.STRING,
  districtName: Sequelize.STRING,
  streetName: Sequelize.STRING,
  streetId: Sequelize.STRING,
  lat: Sequelize.STRING,
  lng: Sequelize.STRING,
  wallType: Sequelize.STRING,
  floor: Sequelize.STRING,
  beautifulUrl: Sequelize.STRING,
});

let uniqueStreets = [];

// getting unique streets
sequelize.query("SELECT DISTINCT `streetName` FROM `flat-analytics-new-11-05s`", { model: Flat } ).then(myTableRows => {
  /*for(let i=0; i < myTableRows.length; i++) {
    uniqueStreets.push({text: myTableRows[i].streetName})
  }*/

  for (let i=0; i < 9000; i++) {
    if (myTableRows[i] && myTableRows[i].streetName) {
      console.log(myTableRows[i].streetName);
      uniqueStreets.push(myTableRows[i].streetName)
    }
  }
  // console.log(myTableRows)
})
  .then(() => fs.writeFile('unique-streets.js', util.inspect(uniqueStreets, { maxArrayLength: null }) , 'utf-8', (error) => { /* handle error */ })
  );

