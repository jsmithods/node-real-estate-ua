// Average price overall
SELECT AVG(`price`) FROM `flat-analytics-new-11-05s`;

// Average price by room
SELECT AVG(`price`) FROM `flat-analytics-new-11-05s` WHERE `rooms`=2;

// Average squareMeters by room
SELECT AVG(`squareMeters`) FROM `flat-analytics-new-11-05s` WHERE `rooms`=2;

// Count flat at exact district
SELECT COUNT(`id`) FROM `flat-analytics-new-11-05s` WHERE `districtName`="Киевский";

// Select only different streets
SELECT DISTINCT `streetName` FROM `flat-analytics-new-11-05s`;

// Select unique values
SELECT DISTINCT `uid` FROM `flat-analytics-new-11-05s`;

// select max price
SELECT `districtName`, `rooms`, COUNT(`id`), AVG(`priceItem`), AVG(`sM`), MIN(`price`), MAX(`price`) FROM `flat-analytics-new-11-05s` GROUP BY `rooms`, `districtName`

//  Aggregate only in district
SELECT COUNT(`id`), AVG(`priceItem`), MIN(`price`), MAX(`price`), `rooms` FROM `flat-analytics-new-11-05s` WHERE `districtName`="Киевский" GROUP BY `rooms`

//  Improved Aggregate only in district
SELECT COUNT(`id`), AVG(`priceItem`) AS `average price sq M`, MIN(`price`) AS `min price`, MAX(`price`) AS `max price`, AVG(`price`) AS `average price`, `rooms`
FROM `flat-analytics-new-11-05s`
WHERE `districtName`="Киевский"
GROUP BY `rooms`

// select max price by district
SELECT `districtName`, `rooms`, COUNT(`id`), AVG(`priceItem`), AVG(`sM`), AVG(`price`), MIN(`price`), MAX(`price`)
FROM `flat-analytics-new-11-05s`
WHERE `districtName`="Киевский" OR `districtName`="Приморский" OR `districtName`="Суворовский" OR `districtName`="Малиновский"
GROUP BY `rooms`, `districtName`