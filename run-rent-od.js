let axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-rent', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// function to download content
let download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`./images-rent/${filename}`)).on('close', callback);
  });
};

computeImagePartUrl = function(url) {
  let start = url.lastIndexOf("-");
  return url.substring(start);
};

const Flat = sequelize.define('flat-rent', {
  uid: Sequelize.STRING,
  type: Sequelize.STRING,
  rooms: Sequelize.STRING,
  livingSquareMeters: Sequelize.STRING,
  kitchenSquareMeters: Sequelize.STRING,
  squareMeters: Sequelize.STRING,
  price: Sequelize.STRING,
  description: Sequelize.TEXT,
  streetName: Sequelize.STRING,
  wallType: Sequelize.STRING,
  floor: Sequelize.STRING,
  beautifulUrl: Sequelize.STRING,
});

for (let i = 0; i < 2; i++) {
  setTimeout(() => {
    let searchUrl = `https://dom.ria.com/node/searchEngine/v2/?page=${i}&limit=10&from_realty_id=&to_realty_id=&sort=inspected_sort&user_id=&category=1&realty_type=0&operation_type=3&state_id=12&city_id[20]=12&characteristic[209][from]=&characteristic[209][to]=&characteristic[214][from]=&characteristic[214][to]=&characteristic[216][from]=&characteristic[216][to]=&characteristic[218][from]=&characteristic[218][to]=&characteristic[227][from]=&characteristic[227][to]=&characteristic[228][from]=&characteristic[228][to]=&characteristic[235][from]=&characteristic[235][to]=&characteristic[246]=244&characteristic[1609][from]=&characteristic[1609][to]=&characteristic[1610][from]=&characteristic[1610][to]=&realty_id_only=&date_from=&date_to=&with_phone=&exclude_my=&new_housing_only=&banks_only=&_csrf=mYNnyByp-JWwY8q_6dXRDjaVe0hHAYiOziWc&reviewText=&email=&period=0`;

    let Items = [];
    return axios.get(searchUrl)
      .then(function (response) {
        response.data.items.map(item => Items.push(item));

        for (let t = 0; t < Items.length; t++) {
          setTimeout(() => {
            let flatURL = `https://dom.ria.com/node/searchEngine/v2/view/realty/${Items[t]}?lang_id=2`;
            return axios.get(flatURL)
              .then(function (res) {
                let data = res.data;
                // let imagesArray = [];
                let pictures = res.data.photos;
                let beautifulUrlPhoto = res.data.beautiful_url;

                let imageCount = 0;

                for(let image in pictures) {
                  setTimeout(() => {
                    let urlChunk = computeImagePartUrl(beautifulUrlPhoto);
                    let imageDownloadPath = `https://cdn.riastatic.com/photosnew/dom/photo/${urlChunk}__${image}fl.jpg`;
                    let imageFileName = `${data.realty_id}--${image}.jpg`;

                    return download(imageDownloadPath, imageFileName, function() { });
                  }, 150*(imageCount+1));
                  imageCount++;
                }

                // fs.writeFileSync('items.js', util.inspect(imagesArray) , 'utf-8');
                sequelize.sync()
                  .then(() => Flat.create({
                    uid: data.realty_id,
                    type: data.type,
                    rooms: data.rooms_count,
                    squareMeters: data.total_square_meters,
                    price: data.price,
                    description: data.description,
                    streetName: data.street_name,
                    wallType: data.wall_type,
                    floor: data.floor,
                    livingSquareMeters: data.living_square_meters,
                    kitchenSquareMeters: data.kitchen_square_meters,
                    beautifulUrl: data.beautiful_url
                  }));
              })
              .catch(function (error) {
                console.log(error);
              });
          }, 1500*(t+1));

        }

      })
      .catch(function (error) {
        console.log(error);
      });
  }, 4500*i);

}

/*setTimeout(() => {
	fs.writeFileSync('items.js', util.inspect(Items) , 'utf-8');
	return console.log(Items);
}, 10000) */
