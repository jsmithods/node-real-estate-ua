var axios = require("axios");
let fs = require("fs");
let util = require("util");
let request = require("request");
const Sequelize = require('sequelize');

const sequelize = new Sequelize('real-estate-gps', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

// function to download content
let download = function(uri, dirname, filename, callback){
  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(`${dirname}/${filename}`)).on('close', callback);
  });
};

computeImagePartUrl = function(url) {
  let start = url.lastIndexOf("-");
  return url.substring(start);
};

const Flat = sequelize.define('flat-gps', {
  uid: Sequelize.STRING,
  type: Sequelize.STRING,
  rooms: Sequelize.FLOAT,
  lSM: Sequelize.FLOAT,
  kSM: Sequelize.FLOAT,
  sM: Sequelize.FLOAT,
  price: Sequelize.INTEGER,
  priceItem: Sequelize.FLOAT,
  districtName: Sequelize.STRING,
  streetName: Sequelize.STRING,
  streetId: Sequelize.STRING,
  lat: Sequelize.STRING,
  lng: Sequelize.STRING,
  wallType: Sequelize.STRING,
  floor: Sequelize.FLOAT,
  beautifulUrl: Sequelize.STRING,
  description: Sequelize.TEXT,
});

const FlatImage = sequelize.define('flat-image', {
  flatId: Sequelize.STRING,
  imageName: Sequelize.STRING,
});

for (let i = 0; i < 5; i++) {
  setTimeout(() => {
    let searchUrl = `https://dom.ria.com/node/searchEngine/v2/?with_map=1&page=${i}&limit=20&from_realty_id=&to_realty_id=&sort=inspected_sort&user_id=&category=1&realty_type=2&operation_type=1&state_id=12&characteristic%5B209%5D%5Bfrom%5D=&characteristic%5B209%5D%5Bto%5D=&characteristic%5B214%5D%5Bfrom%5D=&characteristic%5B214%5D%5Bto%5D=&characteristic%5B216%5D%5Bfrom%5D=&characteristic%5B216%5D%5Bto%5D=&characteristic%5B218%5D%5Bfrom%5D=&characteristic%5B218%5D%5Bto%5D=&characteristic%5B227%5D%5Bfrom%5D=&characteristic%5B227%5D%5Bto%5D=&characteristic%5B228%5D%5Bfrom%5D=&characteristic%5B228%5D%5Bto%5D=&characteristic%5B1607%5D%5Bfrom%5D=&characteristic%5B1607%5D%5Bto%5D=&characteristic%5B1608%5D%5Bfrom%5D=&characteristic%5B1608%5D%5Bto%5D=&characteristic%5B234%5D%5Bfrom%5D=&characteristic%5B234%5D%5Bto%5D=&characteristic%5B242%5D=239&characteristic%5B265%5D=0&realty_id_only=&date_from=&date_to=&with_phone=&exclude_my=&new_housing_only=&banks_only=&_csrf=xaxsjBpL-r_SgRE2GRxmcgUQPENE37cBwiJo&reviewText=&email=&period=0`;

    let Items = [];
    return axios.get(searchUrl)
      .then(function (response) {
        response.data.items.map(item => Items.push(item));

        for(let t = 0;t < Items.length;t++) {
          setTimeout(() => {
            let flatURL = `https://dom.ria.com/node/searchEngine/v2/view/realty/${Items[t]}?lang_id=2`;
            return axios.get(flatURL)
              .then(function (res) {
                let data = res.data;
                // let imagesArray = [];
                let pictures = res.data.photos;
                let beautifulUrlPhoto = res.data.beautiful_url;

                let imageCount = 0;

                let flatPath = `./images-gps/${data.realty_id}`;
                if (!fs.existsSync(flatPath)){
                  fs.mkdirSync(flatPath);
                }

                for(let image in pictures) {
                  setTimeout(() => {
                    let urlChunk = computeImagePartUrl(beautifulUrlPhoto);
                    let imageDownloadPath = `https://cdn.riastatic.com/photosnew/dom/photo/${urlChunk}__${image}fl.jpg`;
                    let imageFileName = `${data.realty_id}-${image}.jpg`;

                    sequelize.sync()
                      .then(() => FlatImage.create({
                        flatId: data.realty_id,
                        imageName: image,
                      }));

                    return download(imageDownloadPath, flatPath, imageFileName, function() { });
                  }, 100*(imageCount+1));
                  imageCount++;
                }

                // fs.writeFileSync('items.js', util.inspect(imagesArray) , 'utf-8');
                sequelize.sync()
                  .then(() => Flat.create({
                    uid: data.realty_id,
                    type: data.type,
                    rooms: +data.rooms_count,
                    sM: +data.total_square_meters,
                    price: +data.price,
                    priceItem: +data.price_item,
                    districtName: data.district_name,
                    streetName: data.street_name,
                    streetId: data.street_id,
                    lat: data.latitude,
                    lng: data.longitude,
                    wallType: data.wall_type,
                    floor: data.floor,
                    lSM: data.living_square_meters,
                    kSM: data.kitchen_square_meters,
                    beautifulUrl: data.beautiful_url,
                    description: data.description
                  }));
              })
              .catch(function (error) {
                console.log(error);
              });
          }, 1000*(t+1))

        }

      })
      .catch(function (error) {
        console.log(error);
      });
  }, 4500*i);

}

/*setTimeout(() => {
	fs.writeFileSync('items.js', util.inspect(Items) , 'utf-8');
	return console.log(Items);
}, 10000) */
